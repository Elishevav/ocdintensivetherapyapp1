//
//  Strings.swift
//  OCDIntensiveTherapyApp
//
//  Created by Elisheva Vakrat on 21/08/2019.
//  Copyright © 2019 Meytal Gur. All rights reserved.
//

import Foundation
struct Strings{
    static let Segue_To_MainViewController = "Segue_To_MainViewController"
    static let Segue_To_LoginVC = "Segue_To_LoginVC"
//    static let Segue_To_VideoVC = "Segue_To_VideoVC"
}
