//
//  AppData.swift
//  OCDIntensiveTherapyApp
//
//  Created by Vladimir Sarkisyan on 31/08/2019.
//

import UIKit

class AppData: NSObject {
    
    static let shared = AppData()
    
    let privacyPolicyURL = "https://www.digitalmarketerhk.com/optin31856534"
    var videos = [Video]()
    let numRows = 3
    let numColumns = 4
    
    private override init() {
        super.init()
        
        setupVideos()
    }
    
    
    func setupVideos() {
        
        videos.append(Video(isLocked: false, URL: "https://player.vimeo.com/video/355191402", backgrounImage: "Video1"))
        videos.append(Video(isLocked: false, URL: "https://player.vimeo.com/video/355192282", backgrounImage: "Video2"))
        videos.append(Video(isLocked: false, URL: "https://player.vimeo.com/video/355193075", backgrounImage: "Video3"))
        videos.append(Video(isLocked: true, URL: "https://player.vimeo.com/video/355648440", backgrounImage: "Video4"))
        videos.append(Video(isLocked: true, URL: "https://player.vimeo.com/video/355648440", backgrounImage: "Video5"))
        videos.append(Video(isLocked: true, URL: "https://player.vimeo.com/video/355648440", backgrounImage: "Video6"))
        videos.append(Video(isLocked: true, URL: "https://player.vimeo.com/video/355648440", backgrounImage: "Video7"))
        videos.append(Video(isLocked: true, URL: "https://player.vimeo.com/video/355648440", backgrounImage: "Video8"))
        videos.append(Video(isLocked: true, URL: "https://player.vimeo.com/video/355648440", backgrounImage: "Video9"))
        videos.append(Video(isLocked: true, URL: "https://player.vimeo.com/video/355648440", backgrounImage: "Video10"))
        videos.append(Video(isLocked: true, URL: "https://player.vimeo.com/video/355648440", backgrounImage: "Video11"))
        videos.append(Video(isLocked: true, URL: "https://player.vimeo.com/video/355648440", backgrounImage: "Video12"))
    }
}
