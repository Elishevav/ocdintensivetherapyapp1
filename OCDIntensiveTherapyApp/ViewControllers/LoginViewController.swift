//
//  LoginViewController.swift
//  OCDIntensiveTherapyApp
//
//

import UIKit
import FirebaseAuth
    
class LoginViewController: UIViewController {
    
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var userEmail: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var policyTextView: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupScreen()
        
        userName.delegate = self
        userEmail.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func setupScreen(){
        let attributedString = NSMutableAttributedString(string: "By clicking \"Reveal the Secrets NOW\" Btn you accept the Privacy Policy of This App")
        // center alignment
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        attributedString.addAttributes([NSAttributedString.Key.paragraphStyle: paragraph], range: NSRange(location: 0, length: attributedString.length))
        // link
        attributedString.addAttribute(.link, value: AppData.shared.privacyPolicyURL, range: NSRange(location: 56, length: 14))
        policyTextView.attributedText = attributedString
        
        policyTextView.delegate = self
    }
    
    // MARK: PRIVATE METHODS
    
    func createUser() {
        
        if let email = self.userEmail.text, let userName = self.userName.text {
            let nameu = userName + "ocdTh" 
            //self.email.text is exists
            Auth.auth().createUser(withEmail: email, password: nameu) { [weak self] (result, error) in
                if let error = error {
                    self?.handleSignUPError(email: email, nameu: nameu, error: error)
                }
                else{ // SIGN UP SUCCESS
                    if let _res = result {
                        print(_res)
                        self?.openMainVC()
                    }
                }
            }
        }
    }
    
    func handleSignUPError(email: String, nameu: String, error: Error) {
        if let errCode = AuthErrorCode(rawValue: error._code) {
            print(errCode)
            // CASE EXISTED USER
            if errCode == .emailAlreadyInUse {
                debugPrint("Email allready in use")
                // Try to sign in when the account allready exist
                Auth.auth().signIn(withEmail: email, password: nameu) { [weak self] user, error in
                    guard let strongSelf = self else { return }
                    if let error = error {
                        strongSelf.showAlert(title: "warning", message: error.localizedDescription, handlerOK:
                            { (alert) in
                        })
                        return
                    }
                    else {
                        strongSelf.openMainVC()
                    }
                }
            }
            else {
                    showAlert(title: "warning", message: error.localizedDescription, handlerOK:
                    { (alert) in })
            }
            //                        print(error.localizedDescription)
        }
        
    }
    
    
    func checkIfUserLoggedIn() -> Bool {
        if Auth.auth().currentUser != nil {
            return true
        }
        return false
    }
    
    
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    
    func openMainVC() {
        //                            self.performSegue(withIdentifier: Strings.Segue_To_MainViewController, sender: nil)
        let appDelegate: AppDelegate = UIApplication.shared.delegate! as! AppDelegate
        appDelegate.window?.rootViewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "MainViewControllerID")
    }
    
    // MARK: IBACTIONS
    
    @IBAction func LoginButtonAction(_ sender: Any) {
        
        if (userEmail.text == "" && userName.text == "" )
        {
            self.showAlert(title: "warning", message: "Please fill Email and Username, all fields are requerd", handlerOK:
                { (alert) in
            })
        }else if(userName.text!.count < 1)
        {      self.showAlert(title: "warning", message: "Please enter your name:)", handlerOK:
            { (alert) in
        })
            
        }
        else if( self.isValidEmail(testStr: userEmail.text!) == false){
            self.showAlert(title: "warning", message: "An email is invalid please try again", handlerOK:
                { (alert) in
            })
        }
        else {
            //  (sender as AnyObject).setBackgroundImage(UIImage(named: "START_green"), for: UIControl.State.normal)
            createUser()
        }
    }
    
   
}


extension LoginViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        UIApplication.shared.open(URL)
        return false
    }
}


extension LoginViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == userName {
            userEmail.becomeFirstResponder()
        }
        else { // case was email textfield
            userEmail.resignFirstResponder()
        }
        
        return true
    }
}
