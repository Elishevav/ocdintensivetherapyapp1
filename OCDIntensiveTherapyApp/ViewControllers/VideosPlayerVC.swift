//
//  VideosPlayerVC.swift
//  OCDIntensiveTherapyApp
//
//  Created by Vladimir Sarkisyan on 31/08/2019.
//

import UIKit
import AVKit
import WebKit


class VideosPlayerVC: UIViewController {
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var backBttn: UIButton!

     var video : Video?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        backBttn.layer.cornerRadius = 5.0
        loadVideo()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        loadVideo()
    }
    
    func loadVideo()
    {
        
        if let urlStr = video?.URL{
            
            let embedHTML="<html><head><style type=\"text/css\">body {background-color: black;color: black;}</style><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=yes\"/></head><body style=\"margin:0\"><div><iframe src=\"\(urlStr)\" frameborder=\"0\" allow=\"autoplay; fullscreen\" allowfullscreen style=\"overflow:hidden; width:100%; height:100%;\"  height=\"100%\" width=\"100%\"></iframe></div></body></html>"
            
            debugPrint(embedHTML)
            let url = URL(string: "https://")!
            webView.loadHTMLString(embedHTML as String, baseURL:url )
            webView.contentMode = .scaleToFill
        }
    }
    
    @IBAction func BackButton(_ sender: Any) {
        webView = nil
        self.dismiss(animated: true)
    }
    
}
    
