//
//  MainViewController.swift
//  OCDIntensiveTherapyApp
//
//  Created by Vladimir Sarkisyan on 31/08/2019.
//

import UIKit
import Firebase
import AVKit

    
    class MainViewController: UIViewController{

        @IBOutlet weak var contentView: UIView!
        @IBOutlet weak var verticalStackView: UIStackView!
        
        var videoList = [Video]()
        var arrayOfIDs = [String]()
        let avPlayerController = AVPlayerViewController()

        var chosenVideo: Video?
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            setupScreen()
        }
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            
//            setupScreen()
        }
        
        func setupScreen() {
            debugPrint("setup MainViewController")
            verticalStackView.distribution = .fillEqually
            verticalStackView.spacing = 20
            var videoNdx = 0
            
            for _ in 0..<AppData.shared.numRows {
                let hStackView = UIStackView(arrangedSubviews: [])
                hStackView.distribution = .fillEqually
                hStackView.spacing = 10
                
                verticalStackView.addArrangedSubview(hStackView)

                
                for _ in 0..<AppData.shared.numColumns {
                    if let cellView: VideoCellView = Bundle.main.loadNibNamed("VideoCellView", owner: self, options: nil)?.first as? VideoCellView
                    {
                        let video = AppData.shared.videos[videoNdx]
                        cellView.video = video
                        hStackView.addArrangedSubview(cellView)
                        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
                        cellView.addGestureRecognizer(tap)
                        videoNdx += 1
                    }
                }
            }
        }
        
        @objc func handleTap(_ sender: UITapGestureRecognizer?) {
            // handling code
            if let cellView = sender?.view as? VideoCellView,
                let video = cellView.video{
                debugPrint("url:  \(video.URL)")
                chosenVideo = video
                performSegue(withIdentifier: "segue_to_player", sender: self)
            }
        }
        
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

            if let segueName = segue.identifier,
                segueName == "segue_to_player" {
                let playerVC  = segue.destination as! VideosPlayerVC
                playerVC.video = chosenVideo
            }
        }
        
}


