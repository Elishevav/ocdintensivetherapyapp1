//
//  Firebase.swift
//  OCDIntensiveTherapyApp
//
//  Created by Elisheva Vakrat on 21/08/2019.
//  Copyright © 2019 Meytal Gur. All rights reserved.
//

import Foundation
import FirebaseAuth

class FirebaseAuth{
    
    
    static func isUserLoggedIn() -> Bool {
        if Auth.auth().currentUser != nil {
            return true
        }
        return false
    }
    
    static func signOut(){
        do {
            try Auth.auth().signOut()
            print("signed out")
        } catch let err {
            print(err)
        }
    }
    
}

