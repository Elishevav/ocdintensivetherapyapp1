//
//  VideoCellView.swift
//  OCDIntensiveTherapyApp
//
//  Created by Vladimir Sarkisyan on 31/08/2019.
//

import UIKit

class VideoCellView: UIView {
    
    @IBOutlet weak var imageView: UIImageView!
    var video: Video? = nil {
        didSet {
            if let imgName = video?.backgrounImage {
                debugPrint("Setting image \(imgName)")
                imageView.image = UIImage(named: imgName)
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
