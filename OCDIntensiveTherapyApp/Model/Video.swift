//
//  Video.swift
//  OCDIntensiveTherapyApp
//
//  Created by Vladimir Sarkisyan on 31/08/2019.
//

import Foundation

struct Video {
    
    var isLocked : Bool
    var URL : String // for embeded link
    var backgrounImage: String
    
    init(isLocked:Bool, URL: String, backgrounImage:String) {
        
        
        self.isLocked = isLocked
        self.URL = URL
        self.backgrounImage = backgrounImage
        
    }
}
